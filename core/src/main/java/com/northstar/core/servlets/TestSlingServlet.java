package com.northstar.core.servlets;

import java.io.IOException;
import java.util.Map.Entry;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;

import org.json.JSONException;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//@SlingServlet(paths="/bin/trainingproject/testservlet")  --Felix SCR Annotation

//OSGI Declarative Service Annotation
@Component(
		service=Servlet.class,
		property={
				"sling.servlet.paths="+ "/bin/trainingproject/testservlet"
		}
)

public class TestSlingServlet extends SlingSafeMethodsServlet {
	
	private static final long serialVersionUID = 1L;
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
    	
	    String resourcePath = "/content/owners/en/jcr:content/root/responsivegrid/title";

	    ResourceResolver resourceResolver = request.getResourceResolver();
	    Resource res = resourceResolver.getResource(resourcePath);	
	    
	    ValueMap properties = res.adaptTo(ValueMap.class);
	    //You can also access the properties through the ResourceUtil utility class
	    ValueMap properties2 = ResourceUtil.getValueMap(res);
	    
	    //convert valueMap to jsonObject
		JSONObject jsonObject = new JSONObject();
        for( Entry<String, Object> entry : properties.entrySet() ) {
            String key = entry.getKey();
            Object value = entry.getValue();
            try {
				jsonObject.put(key, value);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		
		response.setHeader("Content-Type", "text/html");
    	response.getWriter().print("<h1>Sling Servlet Called</h1>");

    	response.getWriter().print("<h1>Path : "+res.getPath()+"</h1>"); 
    	response.getWriter().print("<h1>name :"+res.getName()+"</h1>" ); 
    	response.getWriter().print("<h1>title :"+properties.get("jcr:title", (String) null)+"</h1>" ); 
    	response.getWriter().print("<h1>resourceType :"+properties2.get("sling:resourceType", (String) null)+"</h1>" ); 
    	response.getWriter().print("<h1>jsonData :"+jsonObject+"</h1>" ); 

    	response.getWriter().close();
		resourceResolver.close();
		
	    logger.debug("request for {} logger test", request.getResource().getPath());

    }   
}

//https://sling.apache.org/documentation/tutorials-how-tos/getting-resources-and-properties-in-sling.html
https://helpx.adobe.com/experience-manager/using/persisting-cq-data-java-content1.html#CreateanExperienceManagerapplicationfolderstructure